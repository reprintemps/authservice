package me.reprintemps.gnu.auth.repo;

import me.reprintemps.gnu.auth.mod.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author : reprintemps
 * - User repo 层的jpa实现
 */
public interface UserRepoJpa extends JpaRepository<UserEntity, Integer>, JpaSpecificationExecutor<UserEntity> {}
