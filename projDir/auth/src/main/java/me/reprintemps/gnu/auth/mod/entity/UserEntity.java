package me.reprintemps.gnu.auth.mod.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author : reprintemps
 * - 用户表对应实体， 用户信息在各个项目内自行存储
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Entity(name = "user")
@Table(name = "user")
@Where(clause = "deleted = false")
public class UserEntity extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "uid", unique = true)
    private String uid;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "deleted")
    private Boolean deleted = false;
}
