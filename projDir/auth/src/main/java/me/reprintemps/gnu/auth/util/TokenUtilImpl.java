package me.reprintemps.gnu.auth.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import me.reprintemps.gnu.auth.mod.TokenClaimDto;
import me.reprintemps.gnu.auth.mod.dto.TokenDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * @author : reprintemps
 * - token util 的默认实现(选用auth0)
 */
@Component
public class TokenUtilImpl implements TokenUtil{

    private final String separator = "&";
    @Value("${auth.config.secret}")
    private String secret;
    @Value("${auth.config.author}")
    private String author;
    @Value("${auth.config.expire}")
    private Long expire;


    /**
     * @param tokenStr : 用于验证的token 编码字符串
     * @param tokenClaimDto : token的条件dto
     * @return : int, 1为正确通过， 0未未定位异常， 1为未通过验证异常
     */
    @Override
    public int validToken(String tokenStr, TokenClaimDto tokenClaimDto) {
        JWT.require(getAlgorithm())
                .withIssuer(author)
                .withClaim("username", tokenClaimDto.getUsername())
                .withArrayClaim("roles", "admin")
                .build().verify(tokenStr);
        return 1;
    }

    @Override
    public int validToken(String tokenStr){
        JWT.require(getAlgorithm())
                .withIssuer(author)
                .build().verify(tokenStr);
        return 1;
    }

    /**
     * @param claimMap: 属性map(auth0设计的真差..不能直接填入 map 好像，接受的 val 也十分有限)
     * @return : String, 返回构造好的 JWT String
     * - 要求:
     *   - 必须用 `,` 分割拼接 roles
     *   - 按照 username, roles 来命名 map
     */
    @Deprecated
    @Override
    public String getToken(Map<String, String> claimMap){
        return JWT.create()
                .withIssuer(author)
                .withIssuedAt(new Date())
                // TODO: 暂时取消过期行为，通过缓存过期来限制时效
                // .withExpiresAt(new Date(System.currentTimeMillis() + expire))
                .withClaim("username", claimMap.get("username"))
                .withArrayClaim("roles", claimMap.get("roles").split(","))
                .sign(getAlgorithm());
    }

    @Override
    public String getToken(TokenDto tokenDto) {
        final ArrayList<String> tmpList = new ArrayList<>();
        tokenDto.getRoles().forEach((k, v) -> tmpList.add(k.toString().concat(separator).concat(v)));
        final String[] roles = tmpList.toArray(new String[tmpList.size()]);
        return JWT.create()
                .withIssuer(author)
                .withIssuedAt(new Date())
                .withClaim("uid", tokenDto.getUid())
                .withArrayClaim("roles", roles)
                .sign(getAlgorithm());
    }

    @Override
    public Map<String, Object> parseToken(String tokenStr){
        final DecodedJWT decode = JWT.decode(tokenStr);
        final HashMap<String, Object> resMap = new HashMap<>();
        resMap.put("uid", decode.getClaim("uid").asString());
        resMap.put("roles", decode.getClaim("roles").asArray(String.class));
        return resMap;
    }

    // 获取算法实例
    public Algorithm getAlgorithm(){
        Algorithm algorithm = null;
        try {
            algorithm = Algorithm.HMAC256(secret);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return algorithm;
    }

    @Override
    public List<String> parseStr2List(String listStr, String separator) {
        return Arrays.asList(listStr.split(separator));
    }
}
