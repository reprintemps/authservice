package me.reprintemps.gnu.auth.serv;

import lombok.extern.slf4j.Slf4j;
import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.mod.dto.LoginFormDto;
import me.reprintemps.gnu.auth.mod.dto.TokenDto;
import me.reprintemps.gnu.auth.mod.entity.UserEntity;
import me.reprintemps.gnu.auth.mod.entity.UserRoleMapEntity;
import me.reprintemps.gnu.auth.repo.UserRepoJpa;
import me.reprintemps.gnu.auth.repo.UserRoleMapRepoJpa;
import me.reprintemps.gnu.auth.util.CacheUtil;
import me.reprintemps.gnu.auth.util.RandomNumberUtil;
import me.reprintemps.gnu.auth.util.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @author : reprintemps
 * - 初版的默认登录服务实现
 */
@Slf4j
@Service
public class LoginServImpl implements LoginServ{

    private final String tokenKey= "uid";
    @Autowired
    private Res res;
    @Autowired
    private RandomNumberUtil randomNumberUtil;
    @Autowired
    private CacheUtil<String> cacheUtil;
    @Autowired
    private TokenUtil tokenUtil;
    @Autowired
    private UserRepoJpa userRepoJpa;
    @Autowired
    private UserRoleMapRepoJpa userRoleMapRepoJpa;

    @Override
    public Res logout(String uid) {
        final int delete = cacheUtil.delete(uid);
        return delete == 1 ? res.success() : res.fail();
    }

    @Override
    public Res validToken(String tokenStr){
        final int i = tokenUtil.validToken(tokenStr);
        // 增加改变验证服务的行为， 防篡改照做，但是有没有(cache中)也要做
        final int i1 = validTokenExist(tokenStr);
        return (i + i1) == 2 ? res.success() : res.fail();
    }
    // TODO : 改变的验证行为(想办法合并这两个行为)
    private int validTokenExist(String tokenStr){
        final String uid = Optional.of((String) tokenUtil.parseToken(tokenStr).get("uid")).get();
        return Objects.isNull(cacheUtil.get(uid)) ? -1 : 1;
    }

    // 更新 token 的过期时限(不再需要了，现在 ehcache 过期策略是访问)
    @Override
    public Res updateTokenExpire(String token) {
        return null;
    }

    @Override
    public Res getTokenByAuthCode(String authCode) {
        final String token = cacheUtil.get(authCode);
        // 拿不到token不用额外处理，到验证不通过即可
        return res.success().setData(token);
    }

    @Override
    public Res getAuthCode(LoginFormDto form) {
        final Res login = login(form.getUsername(), form.getPassword());
        if(login.getCode().equals(1)){
            // 生成一个随机码，然后建立{code: token}的临时存储
            // 然后改变login result的内容，否则返回源失败信息
            final String key = randomNumberUtil.getRandomNumber();
            Optional.ofNullable(login.getData()).ifPresent(any -> {
                // TODO 这个toStr必须改动为得到json的方式，或者那个令牌可以直接toStr
                // TempCache.authCode2TokenMap.put(key, login.getData().toString());
                final int post = cacheUtil.post(key, login.getData().toString());
                assert post == 1;
                // TODO : 伪切面行为
                saveTokenByUid(login.getData().toString());
                login.setData(key);
            });
        }
        return login;
    }
    // TODO: 改写成切面, 当生成了 {授权码: token} 时，同时解析一下给一个用 uid 作为 key 的
    private void saveTokenByUid(String tokenStr){
        final Map<String, Object> tokenMap = tokenUtil.parseToken(tokenStr);
        log.debug("saveTokenByUid(): 伪切面行为触发， 存储{uid: token}, {}", tokenMap.get(tokenKey));
        Optional.ofNullable((String)tokenMap.get("uid")).ifPresent(any -> {
            final int postAff = cacheUtil.post(any, tokenStr);
            assert postAff == 1;
        });
    }

    // 通过username 和 password 登录， 包含令牌
    @Override
    public Res login(String username, String password) {
        final UserEntity userEntity = userRepoJpa.findOne(Example.of(new UserEntity().setUsername(username).setPassword(password))).orElseGet(UserEntity::new);
        if(Objects.nonNull(userEntity.getId())){
            // 伪切面切入点
            final String tokenStr = Optional.ofNullable(issueTokenIfNonExist(userEntity.getUid()))
                    .orElse(tokenUtil.getToken(getTokenDtoListByUserEntity(userEntity)));
            // final String token = tokenUtil.getToken(getUsernameAndRolesMapByUserUid(userEntity));
            return res.success().setData(tokenStr);
        }else {
            return res.fail();
        }

    }
    // 伪切面行为，如果已经存在token那么不再新生成, 或者说扩展一个谓词接口即可
    private String issueTokenIfNonExist(String uid){
        //是不是应该重写 ，随机码用 uuid 合适吗
        final String token = cacheUtil.get(uid);
        if(Objects.nonNull(token)){
            return token;
        }else{
            return null;
        }
    }

    // 获取用户名和角色map， 通过给定的实体
    @Deprecated
    private Map<String, String> getUsernameAndRolesMapByUserUid(UserEntity userEntity){
        final String uid = userEntity.getUid();
        final List<String> ridList = userRoleMapRepoJpa.findAll(Example.of(new UserRoleMapEntity().setUid(uid)))
                .parallelStream().parallel().map(UserRoleMapEntity::getRid).collect(Collectors.toList());
        final HashMap<String, String> resMap = new HashMap<>();
        resMap.put(tokenKey, uid);
        AtomicReference<String> rolesStr = new AtomicReference<>("");
        ridList.parallelStream().forEach(el -> {
            rolesStr.set(rolesStr.get().concat(el).concat(","));
        });
        resMap.put("roles", rolesStr.get());
        return resMap;
    }
    private TokenDto getTokenDtoListByUserEntity(UserEntity userEntity){
        final String uid = userEntity.getUid();
        final IdentityHashMap<Integer, String> tmpMap = new IdentityHashMap<>();
        userRoleMapRepoJpa.findAll(Example.of(new UserRoleMapEntity().setUid(userEntity.getUid())))
                .forEach(el -> tmpMap.put(el.getProjId(), el.getRid()));
        return new TokenDto().setUid(uid).setRoles(tmpMap);
    }
}
