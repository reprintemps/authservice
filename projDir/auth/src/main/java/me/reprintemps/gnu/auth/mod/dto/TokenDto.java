package me.reprintemps.gnu.auth.mod.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.IdentityHashMap;

/**
 * @author : reprintemps
 */
@Data
@Accessors(chain = true)
public class TokenDto {

    private String uid;

    // projId: rid 的角色列表
    private IdentityHashMap<Integer, String> roles;
}
