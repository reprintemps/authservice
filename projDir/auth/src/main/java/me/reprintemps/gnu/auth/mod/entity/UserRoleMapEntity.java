package me.reprintemps.gnu.auth.mod.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author : reprintemps
 * - 用户角色关联实体
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Entity(name = "user_role_map")
@Table(name = "user_role_map")
@Where(clause = "deleted = false")
public class UserRoleMapEntity extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "uid")
    private String uid;

    @Column(name = "rid")
    private String rid;

    // 暂时把这个关联关系写在这里，因为如果写在 role 里， 需要再一次去查询，直接在get rid 时区分可以减少一次 query
    // 隶属于哪个项目的角色
    @Column(name = "proj_id")
    private Integer projId;

    @Column(name = "deleted")
    private Boolean deleted = false;
}
