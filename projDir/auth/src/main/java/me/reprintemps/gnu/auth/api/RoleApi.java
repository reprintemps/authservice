package me.reprintemps.gnu.auth.api;

import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.serv.RoleServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;

@Deprecated
public class RoleApi {

    @Autowired
    private Res res;
    @Autowired
    private RoleServ roleServ;

    // @GetMapping("/roles/_by/token/{token}/_projId/{projId}")
    public Res rolesByToken(@PathVariable("token") String token, @PathVariable("projId") Integer projId){
        return roleServ.rolesByToken(token, projId);
    }
}
