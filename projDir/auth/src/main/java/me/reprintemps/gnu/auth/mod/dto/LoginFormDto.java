package me.reprintemps.gnu.auth.mod.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author : reprintemps
 * - 登录表单的dto
 */
@Data
@Accessors(chain = true)
public class LoginFormDto {

    private String username;

    private String password;
}
