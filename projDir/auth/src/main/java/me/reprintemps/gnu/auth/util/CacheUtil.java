package me.reprintemps.gnu.auth.util;

/**
 * @author : reprintemps
 * - 缓存操作工具接口定义(按照 http 动词约定接口)
 * - 实现时请传入GetType 类型参数
 */
public interface CacheUtil<V> {

    // 新建Cache 记录
    int post(String key, String value);

    // 获取 Cache 记录
    V get(String key);

    // 删除 Cache 记录
    int delete(String key);
}
