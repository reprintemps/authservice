package me.reprintemps.gnu.auth.serv;

import me.reprintemps.gnu.auth.mod.dto.LoginFormDto;
import me.reprintemps.gnu.auth.mod.Res;

/**
 * @author : reprintemps
 * - 登录相关服务 interface 定义
 */
public interface LoginServ {

    // TODO : 推出登录暂时给的是 uid， 是不是应该 token 来确保不会被伪造
    Res logout(String uid);

    // 验证有无伪造过期即可
    Res validToken(String tokenStr);

    // 接续 token， 每次操作来这个项目请求一下，更新项目的 token 时限(写给每次请求还是想办法让 redis 的 hook 来更新？)
    // 用 header 携带
    Res updateTokenExpire(String token);

    // 获取 token 通过授权码
    Res getTokenByAuthCode(String authCode);

    // 获取授权码，参数必须 <? extends LoginFormDto>
    Res getAuthCode(LoginFormDto form);

    // 登录
    Res login(String username, String password);
}
