package me.reprintemps.gnu.auth.mod;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author : reprintemps
 * - token 的 claim 定义字段
 */
@Data
@Accessors(chain = true)
public class TokenClaimDto {

    private String username;

    private List<String> roles;
}
