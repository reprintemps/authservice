package me.reprintemps.gnu.auth.serv;

import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.mod.dto.RegisterFormDto;
import me.reprintemps.gnu.auth.mod.entity.UserEntity;
import me.reprintemps.gnu.auth.mod.entity.UserRoleMapEntity;
import me.reprintemps.gnu.auth.repo.UserRepoJpa;
import me.reprintemps.gnu.auth.repo.UserRoleMapRepoJpa;
import me.reprintemps.gnu.auth.util.RandomNumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author : reprintemps
 * - 注册服务实现
 */
@Service
public class RegisterServImpl implements RegisterServ{

    @Autowired
    private Res res;
    @Autowired
    private UserRepoJpa userRepoJpa;
    @Autowired
    private UserRoleMapRepoJpa userRoleMapRepoJpa;
    @Autowired
    private RandomNumberUtil randomNumberUtil;

    // 注册服务
    @Override
    public Res register(RegisterFormDto registerFormDto) {
        final UserEntity save = userRepoJpa.save(new UserEntity()
                .setUsername(registerFormDto.getUsername())
                .setPassword(registerFormDto.getPassword())
                .setUid(randomNumberUtil.getUid()));
        Optional.of(save).ifPresent(any -> {
            registerFormDto.getRoleMap().forEach((k,v) -> {
                v.forEach(el -> {
                    // 正常情况下应该不会失败，为了性能考虑，暂时不做统计成功情况
                    userRoleMapRepoJpa.save(new UserRoleMapEntity().setProjId(k).setUid(save.getUid()).setRid(el));
                });
            });
        });
        return res.success();
    }

    /**
     * - 请注意保持将Res.data里存入删除返回的uidList(有关联删除的通知依赖于此)
     */
    @Override
    public Res delete(List<String> uidList) {
        final List<String> resultUidList = updateAllDeletedByUidList(uidList);
        return res.success().setCount(resultUidList.size()).setData(resultUidList);
    }
    // 用 specification 方式的删除
    private List<String> updateAllDeletedByUidList(List<String> uidList){
        // 返回用户列表
        final List<UserEntity> resultUserList = userRepoJpa.saveAll(userRepoJpa.findAll(((root, criteriaQuery, criteriaBuilder) -> {
            final Path<Object> uid = root.get("uid");
            final CriteriaBuilder.In<Object> in = criteriaBuilder.in(uid);
            uidList.parallelStream().forEach(in::value);
            return criteriaBuilder.and(in);
        })).parallelStream().map(el -> el.setDeleted(true)).collect(Collectors.toList()));
        return resultUserList.parallelStream().map(UserEntity::getUid).collect(Collectors.toList());
    }

    // 为切面提供 删除关联的角色记录
    @Override
    public Res deleteRelationRole(List<String> uidList){
        // 如果是多个，则可能造成多个查询语句， 性能也许有问题，需要检查后重写可能
        uidList.parallelStream().forEach(el -> {
            final List<UserRoleMapEntity> elRoleRecord = userRoleMapRepoJpa.findAll(Example.of(new UserRoleMapEntity().setUid(el)));
            final List<UserRoleMapEntity> tmpList = elRoleRecord.parallelStream().map(el1 -> el1.setDeleted(true)).collect(Collectors.toList());
            userRoleMapRepoJpa.saveAll(tmpList);
        });
        // 可能只有成功
        return res.success();
    }

    // 修改用户信息
    @Override
    public Res alter(RegisterFormDto registerFormDto) {
        final UserEntity userSave = userRepoJpa.save(new UserEntity()
                .setUsername(registerFormDto.getUsername())
                .setPassword(registerFormDto.getPassword())
                .setUid(registerFormDto.getUid()));
        List<UserRoleMapEntity> tmpList = new ArrayList<>();
        // 好像替换为 map 直接改变结果 然后存入更简洁优雅
        registerFormDto.getRoleMap().keySet().parallelStream().forEach(el -> {
            registerFormDto.getRoleMap().get(el).parallelStream().forEach(el1 -> {
                tmpList.add(new UserRoleMapEntity().setUid(registerFormDto.getUid()).setProjId(el).setRid(el1));
            });
        });
        final List<UserRoleMapEntity> saveList = userRoleMapRepoJpa.saveAll(tmpList);
        return res.success().setCount(saveList.size());
    }

}
