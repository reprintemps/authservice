package me.reprintemps.gnu.auth.aspect;

import com.auth0.jwt.exceptions.InvalidClaimException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @author : reprintemps
 * - token相关的切面
 *   - 拦截验证异常
 */
@Slf4j
@Aspect
@Component
public class TokenAspect {

    // TODO 为什么不能用占位符代替public
    @Pointcut("execution(public int me.reprintemps.gnu.auth.util.TokenUtilImpl.validToken(..))")
    private void tokenValidPointCut(){}

    // 改变验证 token 验证失败的方法返回值
    @Around(value = "tokenValidPointCut()")
    public int aroundTokenVerifyException(ProceedingJoinPoint proceedingJoinPoint){
        log.debug("aroundTokenVerifyException(): 执行");
        final Object[] args = proceedingJoinPoint.getArgs();
        try {
            final Object proceed = proceedingJoinPoint.proceed(args);
        } catch(InvalidClaimException invalidClaimException){
            log.error("aroundTokenVerifyException(): 验证未通过, {}", invalidClaimException.getMessage());
            return -1;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return  0;
        }
        return 1;
    }
}
