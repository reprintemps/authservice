package me.reprintemps.gnu.auth.config;

import lombok.extern.slf4j.Slf4j;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.MemoryUnit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Assert;

/**
 * @author : reprintemps
 * - token 缓存的配置
 */
@Slf4j
@Configuration
public class TokenCacheConfig {

    private CacheManager cacheManager;
    private final static String cacheDirectory = "d:\\javaCache\\ehcache";

    {
        init();
    }

    private void init(){
        log.info("init(): EhCache init ...");
        final CacheConfiguration<String, String> cacheConfig = CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, String.class,
                ResourcePoolsBuilder.newResourcePoolsBuilder()
                        .heap(500, MemoryUnit.MB).offheap(600, MemoryUnit.MB).disk(700, MemoryUnit.MB, true))
                // TODO : 有没有需要长期在线的，比如他等待下载，这里有一个隐患，大于 30min 则会驱逐该 token， 登录态失效
                .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(java.time.Duration.ofMinutes(30)))
                .build();
        Assert.notNull(cacheDirectory, "配置缓存地址为 null !");
        cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .with(CacheManagerBuilder.persistence(cacheDirectory))
                .withCache("tokenCache", cacheConfig).build();
        cacheManager.init();
    }

    @Bean
    public Cache<String, String> tokenCache(){
        return cacheManager.getCache("tokenCache", String.class, String.class);
    }
}
