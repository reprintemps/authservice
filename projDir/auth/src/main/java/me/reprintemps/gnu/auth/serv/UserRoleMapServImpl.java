package me.reprintemps.gnu.auth.serv;

import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.mod.entity.UserRoleMapEntity;
import me.reprintemps.gnu.auth.repo.UserRoleMapRepoJpa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author : reprintemps
 */
@Service
public class UserRoleMapServImpl implements UserRoleMapServ{

    @Autowired
    private Res res;
    @Autowired
    private UserRoleMapRepoJpa userRoleMapRepoJpa;

    @Override
    public Res delete(List<Integer> idList) {
        final List<UserRoleMapEntity> findById = userRoleMapRepoJpa.findAllById(idList);
        final List<UserRoleMapEntity> alterList = findById.stream().map(el -> el.setDeleted(true)).collect(Collectors.toList());
        final List<UserRoleMapEntity> resultList = userRoleMapRepoJpa.saveAll(alterList);
        return res.success().setCount(resultList.size());
    }

    @Override
    public Res get(UserRoleMapEntity userRoleMap) {
        final List<String> roleList = userRoleMapRepoJpa.findAll(Example.of(new UserRoleMapEntity().setUid(userRoleMap.getUid()).setProjId(userRoleMap.getProjId())))
                .stream().map(UserRoleMapEntity::getRid).collect(Collectors.toList());
        return res.success().setData(roleList).setCount(roleList.size());
    }

    @Override
    public Res post(List<UserRoleMapEntity> userRoleMapList) {
        final List<UserRoleMapEntity> resultList = userRoleMapRepoJpa.saveAll(userRoleMapList);
        return res.success().setCount(resultList.size());
    }

    @Override
    public Res put(List<UserRoleMapEntity> userRoleMapList) {
        userRoleMapList.forEach(el -> {
            // 先查询，如果存在等价关联，则不发生动作，如果没有等价关联就插入即可
            final UserRoleMapEntity tmpEntity = new UserRoleMapEntity().setUid(el.getUid()).setProjId(el.getProjId()).setRid(el.getRid());
            final UserRoleMapEntity tmpMap = userRoleMapRepoJpa.findOne(Example.of(tmpEntity))
                    .orElseGet(UserRoleMapEntity::new);
            if(Objects.isNull(tmpMap.getId())){
                userRoleMapRepoJpa.save(tmpEntity);
            }
        });
        return res.success();
    }
}
