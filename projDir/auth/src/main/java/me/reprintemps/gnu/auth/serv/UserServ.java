package me.reprintemps.gnu.auth.serv;

import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.mod.dto.RegisterFormDto;
import me.reprintemps.gnu.auth.mod.entity.UserEntity;
import org.springframework.data.domain.Pageable;

/**
 * @author : reprintemps
 * @date : 2021-05-22
 */
public interface UserServ {

    // 修改用户
    Res put(UserEntity user);

    // 分页查询用户
    Res getUsers(Pageable pageable);

    // 根据 uid 拿到 user 实体
    Res getUserByUid(String uid);

    // 根据 token 得到 uid
    Res getUidByToken(String token);

    // 注册服务
    Res post(RegisterFormDto registerFormDto);
}
