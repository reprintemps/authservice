package me.reprintemps.gnu.auth.serv;

import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.mod.dto.RegisterFormDto;

import java.util.List;

/**
 * @author : reprintemps
 * - 注册用服务
 */
public interface RegisterServ {

    // 注册用户
    Res register(RegisterFormDto registerFormDto);

    // 批量删除
    Res delete(List<String> uidList);

    // 修改用户
    Res alter(RegisterFormDto registerFormDto);

    // 删除关联的用户角色
    Res deleteRelationRole(List<String> uidList);
}
