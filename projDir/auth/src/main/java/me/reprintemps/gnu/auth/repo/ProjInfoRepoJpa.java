package me.reprintemps.gnu.auth.repo;

import me.reprintemps.gnu.auth.mod.entity.ProjInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author : reprintemps
 * - 项目信息表
 */
public interface ProjInfoRepoJpa extends JpaRepository<ProjInfoEntity, Integer> {}
