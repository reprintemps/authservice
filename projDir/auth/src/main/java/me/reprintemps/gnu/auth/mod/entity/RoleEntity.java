package me.reprintemps.gnu.auth.mod.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * @author : reprintemps
 * - 角色实体
 */
@Deprecated
@Data
@Accessors(chain = true)
// equals 方法的重写
@EqualsAndHashCode(callSuper = true)
// TODO: 暂时把角色表废弃，角色的名字，定义，都由各个项目来定义，这里只记录一个关系 @Entity(name = "role")
@Table(name = "role")
public class RoleEntity extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "rid")
    private String rid;

    @Column(name = "role_name")
    private String roleName;

    @Column(name = "deleted")
    private Boolean deleted = false;
}
