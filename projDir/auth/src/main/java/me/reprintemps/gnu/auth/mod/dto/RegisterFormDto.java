package me.reprintemps.gnu.auth.mod.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

/**
 * @author : reprintemps
 * - 注册表单传输对象
 */
@ApiModel
@Data
@Accessors(chain = true)
public class RegisterFormDto {

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("角色关系描述")
    // 列表[]<项目:角色列表[]>
    private Map<Integer, List<String>> roleMap;

    // * 额外附加项
    private String uid;
}
