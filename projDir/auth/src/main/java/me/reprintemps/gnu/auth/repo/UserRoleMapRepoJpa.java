package me.reprintemps.gnu.auth.repo;

import me.reprintemps.gnu.auth.mod.entity.UserRoleMapEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author : reprintemps
 * - 用户角色关联 repo jpa
 */
public interface UserRoleMapRepoJpa extends JpaRepository<UserRoleMapEntity, Integer> {}
