package me.reprintemps.gnu.auth.aspect;

import lombok.extern.slf4j.Slf4j;
import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.serv.RegisterServ;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author : reprintemps
 * - 业务切面(关联删除数据之类)
 */
@Slf4j
@Aspect
@Component
public class BusinessAspect {

    @Autowired
    private RegisterServ registerServ;

    // 删除用户时的切点
    @Pointcut(value = "execution(public me.reprintemps.gnu.auth.mod.Res me.reprintemps.gnu.auth.serv.RegisterServImpl.delete(..))")
    private void deleteUserPointCut(){};


    // 当删除用户后，还需要关联删除哪些数据
    @SuppressWarnings("unchecked")
    @AfterReturning(pointcut = "deleteUserPointCut()", returning = "deleteResult")
    public void afterDeleteUser(JoinPoint joinPoint, Res deleteResult){
        if(deleteResult.getCode().equals(1)){
            if(deleteResult.getData() instanceof List){
                final List<String> uidList = (List<String>) deleteResult.getData();
                final Res res = registerServ.deleteRelationRole(uidList);
                // 暂时用断言来做删除的判断
                assert  res.getCode().equals(1);
            }
            // 自动根据签名重载的方法

        }
    }
}
