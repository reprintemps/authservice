package me.reprintemps.gnu.auth.serv;

import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.mod.dto.RegisterFormDto;
import me.reprintemps.gnu.auth.mod.entity.UserEntity;
import me.reprintemps.gnu.auth.repo.UserRepoJpa;
import me.reprintemps.gnu.auth.util.RandomNumberUtil;
import me.reprintemps.gnu.auth.util.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @author : reprintemps
 * @date : 2021-05-22
 */
@Service
public class UserServImpl implements UserServ{

    private final String uidKey = "uid";

    @Autowired
    private TokenUtil tokenUtil;
    @Autowired
    private UserRepoJpa userRepoJpa;
    @Autowired
    private RandomNumberUtil randomNumberUtil;
    @Autowired
    private Res res;

    @Override
    public Res put(UserEntity user) {
        final UserEntity userEntity = userRepoJpa.findById(user.getId()).orElseGet(UserEntity::new);
        if(Objects.isNull(userEntity.getId())){
            return res.fail().setMsg("不存在该用户!");
        }
        userEntity.setUsername(Objects.isNull(user.getUsername()) ? userEntity.getUsername() : user.getUsername());
        userEntity.setPassword(Objects.isNull(user.getPassword()) ? userEntity.getPassword() : user.getPassword());
        final UserEntity save = userRepoJpa.save(userEntity);
        return res.success();
    }

    @Override
    public Res getUsers(Pageable pageable) {
        final List<UserEntity> resList = userRepoJpa.findAll(pageable).toList();
        return res.success().setData(resList).setCount(resList.size());
    }

    @Override
    public Res getUserByUid(String uid) {
        final UserEntity userEntity = userRepoJpa.findOne(Example.of(new UserEntity().setUid(uid))).orElseGet(UserEntity::new);
        if(Objects.nonNull(userEntity.getId())){
            return res.success().setData(userEntity);
        }
        return res.fail().setMsg("根据 uid 未查找到该用户.");
    }

    @Override
    public Res getUidByToken(String token) {
        final Map<String, Object> tokenMap = tokenUtil.parseToken(token);
        final String uid = Optional.ofNullable((String) tokenMap.get(uidKey)).orElseThrow(ClassCastException::new);
        return res.success().setData(uid);
    }

    @Override
    public Res post(RegisterFormDto registerFormDto) {
        final UserEntity save = userRepoJpa.save(new UserEntity().setUsername(registerFormDto.getUsername())
                .setPassword(registerFormDto.getPassword())
                .setUid(randomNumberUtil.getUid()));
        return Objects.nonNull(save.getUid()) ? res.success() : res.fail();
    }
}
