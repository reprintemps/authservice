package me.reprintemps.gnu.auth.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.serv.LoginServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author : reprintemps
 * - 授权相关 api
 */
@Api(tags = "认证相关")
@CrossOrigin
@RequestMapping("/auth")
@RestController
public class AuthApi {

    @Autowired
    private LoginServ loginServ;

    // 简单的查该 token 是否可用
    @ApiOperation("验证 token")
    @GetMapping("/_by/token/{token}")
    public Res authByToken(@PathVariable("token") String tokenStr){
        return loginServ.validToken(tokenStr);
    }
}
