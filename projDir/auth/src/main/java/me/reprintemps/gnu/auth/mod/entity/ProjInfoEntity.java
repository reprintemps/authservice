package me.reprintemps.gnu.auth.mod.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * @author : reprintemps
 * - 项目的具体信息
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Entity(name = "proj_info")
@Table(name = "proj_info")
@Where(clause = "deleted = false")
public class ProjInfoEntity extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "proj_id")
    private Integer projId;

    @Column(name = "proj_name")
    private String projName;

    @Column(name = "deleted")
    private Boolean deleted = false;

}
