package me.reprintemps.gnu.auth.serv;

import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.mod.entity.UserRoleMapEntity;

import java.util.List;

/**
 * @author : reprintemps
 */
public interface UserRoleMapServ {

    // 按照 id 删除
    Res delete(List<Integer> idList);

    // 寻找用户角色关联
    Res get(UserRoleMapEntity userRoleMap);

    // 注册用户-角色关联关系
    Res post(List<UserRoleMapEntity> userRoleMapList);

    // 修改用户-角色的关联关系
    Res put(List<UserRoleMapEntity> userRoleMapList);
}
