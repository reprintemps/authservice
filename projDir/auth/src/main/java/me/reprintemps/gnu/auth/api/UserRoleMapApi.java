package me.reprintemps.gnu.auth.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.mod.entity.UserRoleMapEntity;
import me.reprintemps.gnu.auth.serv.UserRoleMapServ;
import me.reprintemps.gnu.auth.util.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@Api(tags = "用户角色关联相关")
@CrossOrigin
@RequestMapping("/user/role")
@RestController
public class UserRoleMapApi {

    @Autowired
    private Res res;
    @Autowired
    private UserRoleMapServ userRoleMapServ;
    @Autowired
    private TokenUtil tokenUtil;

    @ApiOperation(value = "通过 token 和 projId 拿到特定项目的 roles")
    @GetMapping("/roles/_by/token/{token}/_projId/{projId}")
    public Res getByTokenAndProjId(@PathVariable("token") String token, @PathVariable("projId") Integer projId){
        final Map<String, Object> tokenMap = tokenUtil.parseToken(token);
        List<String> roles = new ArrayList<String>(0);
        if(tokenMap.get("roles") instanceof String[]){
            roles = Optional.ofNullable(Arrays.<String>asList((String[])tokenMap.get("roles"))).orElseThrow(ClassCastException::new);
            roles = roles.stream().filter(el -> Integer.valueOf(el.split("&")[0]).equals(projId))
                    .map(el -> el.split("&")[1]).collect(Collectors.toList());
        }
        return res.success().setData(roles).setCount(roles.size());
    }

    @ApiOperation(value = "寻找符合条件的角色关联记录，返回角色id列表")
    @GetMapping("/roles/_by/uid/projId")
    public Res get(UserRoleMapEntity userRoleMap){
        return userRoleMapServ.get(userRoleMap);
    }

    @ApiOperation(value = "注册角色关联的操作", code = 1)
    @PostMapping
    public Res post(@RequestBody List<UserRoleMapEntity> userRoleMapList){
        return userRoleMapServ.post(userRoleMapList);
    }

    @ApiOperation(value = "修改角色关联的操作", code = 1)
    @PutMapping
    public Res put(@RequestBody List<UserRoleMapEntity> userRoleMapList){
        return userRoleMapServ.put(userRoleMapList);
    }

    @ApiOperation(value = "根据 id 删除(批量)")
    @DeleteMapping
    public Res delete(@RequestBody List<Integer> idList){
        return userRoleMapServ.delete(idList);
    }

}
