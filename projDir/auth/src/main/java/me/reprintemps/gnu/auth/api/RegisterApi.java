package me.reprintemps.gnu.auth.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.mod.dto.RegisterFormDto;
import me.reprintemps.gnu.auth.serv.RegisterServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author : reprintemps
 * - 注册用 api
 */
@Api(tags = "注册相关")
@CrossOrigin
@RequestMapping("/register")
@RestController
public class RegisterApi {

    @Autowired
    private RegisterServ registerServ;

    @ApiOperation(value = "通过用户名，密码，角色关系描述注册用户")
    @PostMapping
    public Res register(@RequestBody RegisterFormDto registerFormDto){
        return registerServ.register(registerFormDto);
    }
}
