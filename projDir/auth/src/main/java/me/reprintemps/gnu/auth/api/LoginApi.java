package me.reprintemps.gnu.auth.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.reprintemps.gnu.auth.mod.dto.LoginFormDto;
import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.serv.LoginServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author : reprintemps
 * - 登录相关api
 */
@Api(tags = "登录相关")
@CrossOrigin
@RequestMapping("/login")
@RestController
public class LoginApi {

    @Autowired
    private Res res;
    @Autowired
    private LoginServ loginServ;

    @ApiOperation(value = "主动退出登录态")
    @DeleteMapping("/logout/{uid}")
    public Res logout(@PathVariable("uid") String uid){
        return loginServ.logout(uid);
    }

    // 根据授权码拿到token
    @ApiOperation(value = "根据授权码获取 token")
    @GetMapping("/token/_by/authCode/{authCode}")
    public Res getTokenByAuthCode(@PathVariable("authCode") String authCode){
        return loginServ.getTokenByAuthCode(authCode);
    }

    // 获取授权码，即去缓存提取 token 的 key
    @ApiOperation(value = "获取授权码")
    @PostMapping("/authCode")
    public Res getAuthCode(@RequestBody LoginFormDto form){
        return loginServ.getAuthCode(form);
    }

    // 登录
    public Res login(String username, String password){
        return loginServ.login(username, password);
    }

}
