package me.reprintemps.gnu.auth.serv;

import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.mod.entity.UserRoleMapEntity;
import me.reprintemps.gnu.auth.repo.UserRoleMapRepoJpa;
import me.reprintemps.gnu.auth.util.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author : reprintemps
 */
@Service
public class RoleServImpl implements RoleServ{

    @Autowired
    private TokenUtil tokenUtil;
    private final String roleKey = "roles";
    private final String separator = ",";
    @Autowired
    private UserRoleMapRepoJpa userRoleMapRepoJpa;
    @Autowired
    private Res res;

    @Override
    public Res rolesByToken(String token, Integer projId) {
        final Map<String, Object> tokenMap = tokenUtil.parseToken(token);
        final String tmpStr = Optional.ofNullable((String) tokenMap.get(roleKey)).orElseGet(String::new);
        final List<String> roleList = tokenUtil.parseStr2List(tmpStr, separator);
        // TODO 查询用户关联表里， 项目 id 和 用户 id 是这个的 角色字段
        final String uid = Optional.ofNullable((String) tokenMap.get("username")).orElseThrow(ClassCastException::new);
        final List<String> roleIdList = userRoleMapRepoJpa.findAll(Example.of(new UserRoleMapEntity().setUid(uid).setProjId(projId)))
                .stream().map(UserRoleMapEntity::getRid).collect(Collectors.toList());
        return res.success().setData(roleIdList);
    }
}
