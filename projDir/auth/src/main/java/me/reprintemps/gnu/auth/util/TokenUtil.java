package me.reprintemps.gnu.auth.util;

import me.reprintemps.gnu.auth.mod.TokenClaimDto;
import me.reprintemps.gnu.auth.mod.dto.TokenDto;

import java.util.List;
import java.util.Map;

/**
 * @author : reprintemps
 * - token 工具接口定义
 */
public interface TokenUtil {

    // 实现获取 token 的方法
    String getToken(Map<String, String> claimMap);
    String getToken(TokenDto tokenDto);
    // 解析加密后的 token
    Map<String, Object> parseToken(String tokenStr);

    // 验证 token 是否被篡改
    int validToken(String tokenStr, TokenClaimDto tokenClaimDto);

    int validToken(String tokenStr);

    // 根据给定的分割符号和数组字符串转换为数组
    List<String> parseStr2List(String listStr, String separator);
}
