package me.reprintemps.gnu.auth.serv;

import me.reprintemps.gnu.auth.mod.Res;

/**
 * @author : reprintemps
 */
public interface RoleServ {

    Res rolesByToken(String token, Integer projId);
}
