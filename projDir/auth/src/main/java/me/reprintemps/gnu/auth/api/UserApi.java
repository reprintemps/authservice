package me.reprintemps.gnu.auth.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.mod.dto.RegisterFormDto;
import me.reprintemps.gnu.auth.mod.entity.UserEntity;
import me.reprintemps.gnu.auth.serv.RegisterServ;
import me.reprintemps.gnu.auth.serv.UserServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author : reprintemps
 * - 用户 api
 */
@Api(tags = "用户相关")
@CrossOrigin
@RequestMapping("/user")
@RestController
public class UserApi {

    @Autowired
    private RegisterServ registerServ;
    @Autowired
    private UserServ userServ;

    @ApiOperation(value = "用户全部获取(分页)")
    @GetMapping("/users")
    public Res getUsers(Pageable pageable){
        return userServ.getUsers(pageable);
    }

    @ApiOperation(value = "通过 uid 获取用户")
    @GetMapping("/user/_by/uid/{uid}")
    public Res userByUid(@PathVariable("uid") String uid){
        return userServ.getUserByUid(uid);
    }

    @ApiOperation(value = "根据 token 获取 uid")
    @GetMapping("/uid/_by/token/{tokenStr}")
    public Res getUidByToken(@PathVariable("tokenStr") String tokenStr){
        return userServ.getUidByToken(tokenStr);
    }

    @ApiOperation(value = "删除用户(批量)", code = 1)
    @DeleteMapping("/_by/uidList")
    public Res delete(@RequestBody List<String> uidList){
        return registerServ.delete(uidList);
    }

    @ApiOperation(value = "修改用户")
    @PutMapping
    public Res put(@RequestBody UserEntity userEntity){
        return userServ.put(userEntity);
    }

    @ApiOperation(value = "注册用户", code = 1)
    @PostMapping("/user")
    public Res post(@RequestBody RegisterFormDto registerFormDto){
        return userServ.post(registerFormDto);
    }
}
