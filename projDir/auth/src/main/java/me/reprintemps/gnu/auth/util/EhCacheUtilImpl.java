package me.reprintemps.gnu.auth.util;

import lombok.extern.slf4j.Slf4j;
import org.ehcache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;

/**
 * @author : reprintemps
 * - 缓存工具的 EhCache 实现
 */
@Slf4j
@Component
public class EhCacheUtilImpl implements CacheUtil<String>{

    @Autowired
    private Cache<String, String> tokenCache;

    // TODO 总是返回 1 (应该利用 int 来指示是否 put 成功，以及错误类型)
    @Override
    public int post(String key, String value){
        // TODO 暂时换用putIfAbsent 字面意思应该是不存在才推入，实际还没确定
        tokenCache.putIfAbsent(key, value);
        // tokenCache.put(key, value);
        log.debug("post(): 推入一个新的token, {}", tokenCache.get(key));
        return 1;
    }

    @Override
    public String get(String key){
        tokenCache.getAll(new HashSet<>()).forEach((k, v) -> {
            System.out.println("展示一下kv : ".concat(k.toString()).concat("---").concat(v));
        });
        return tokenCache.get(key);
    }

    @Override
    public int delete(String key) {
        tokenCache.remove(key);
        return 1;
    }
}
