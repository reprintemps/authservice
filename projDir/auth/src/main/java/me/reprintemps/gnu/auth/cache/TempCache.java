package me.reprintemps.gnu.auth.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * @author : reprintemps
 * - 暂时用map做缓存，存储 {临时授权码: token}的缓存
 */
@Deprecated
public class TempCache {
    // 公开的授权码和token Map，之后替换该类 2 redis
    public final static Map<String, String> authCode2TokenMap = new HashMap<>();
}
