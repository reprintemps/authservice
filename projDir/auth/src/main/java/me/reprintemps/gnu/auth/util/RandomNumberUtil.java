package me.reprintemps.gnu.auth.util;

import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * @author : reprintemps
 * - 随机数相关工具
 */
@Component
public class RandomNumberUtil {

    // 获取一个随机数，暂时用原生api
    public String getRandomNumber(){
        return UUID.randomUUID().toString();
    }

    // 获取一个uid (策略自定，暂时用uuid)
    public String getUid(){
        return UUID.randomUUID().toString();
    }
}
