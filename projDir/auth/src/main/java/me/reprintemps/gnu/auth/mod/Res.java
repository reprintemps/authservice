package me.reprintemps.gnu.auth.mod;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;

/**
 * @author : reprintemps
 * - 通过响应对象
 */
@Data
@Accessors(chain = true)
@Component
public class Res {
    private Integer code;
    private Object data;
    private String msg;
    private Integer count;

    // 成功 1， 0预期外
    public Res success(){
        return new Res().setCode(1);
    }

    // 通用失败 -1
    public Res fail(){
        return new Res().setCode(-1);
    }
}
