package me.reprintemps.gnu.auth.util;

import me.reprintemps.gnu.auth.mod.TokenClaimDto;
import org.ehcache.Cache;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@SpringBootTest
public class TokenUtilTest {

    @Autowired
    private TokenUtilImpl tokenUtil;
    @Autowired
    private Cache<String, String> tokenCache;

    @Test
    void testGetToken(){
        final HashMap<String, String> map = new HashMap<>();
        map.put("username", "user");
        final ArrayList<String> roles = new ArrayList<>();
        roles.add("role1");
        roles.add("role2");
        AtomicReference<String> rolesStr = new AtomicReference<>("");
        roles.parallelStream().forEach(el -> {
            rolesStr.set(rolesStr.get().concat(el).concat(","));
        });
        rolesStr.set(rolesStr.get().substring(0, rolesStr.get().length() - 1));
        map.put("roles", rolesStr.get());
        final String token = tokenUtil.getToken(map);
        System.out.println(token);

        final Map<String, Object> stringClaimMap = tokenUtil.parseToken(token);
        stringClaimMap.forEach((k, v) -> {
            System.out.println(k.concat(":::").concat(v.toString()));
        });
        Optional.ofNullable((String[])stringClaimMap.get("roles")).ifPresent(any -> {
            Arrays.stream(any).forEach(System.out::println);
        });

        // 验证是否正确， 具有什么角色
        System.out.println("开始验证...");
        final ArrayList<String> strings = new ArrayList<>();
        strings.add("user0");
        final int user = tokenUtil.validToken(token, new TokenClaimDto().setUsername("user").setRoles(strings));
        System.out.println("抛出异常后，还会继续工作吗".concat(String.valueOf(user)));
    }

    // 该方法通过 jwt.io将结果解码篡改，验证篡改会无法通过验证
    @Test
    void testValidAlterToken(){
        final HashMap<String, String> map = new HashMap<>();
        map.put("username", "user");
        final ArrayList<String> roles = new ArrayList<>();
        roles.add("role1");
        roles.add("role2");
        AtomicReference<String> rolesStr = new AtomicReference<>("");
        roles.parallelStream().forEach(el -> {
            rolesStr.set(rolesStr.get().concat(el).concat(","));
        });
        rolesStr.set(rolesStr.get().substring(0, rolesStr.get().length() - 1));
        map.put("roles", rolesStr.get());

        String token = tokenUtil.getToken(map);
        final int i = tokenUtil.validToken(token);
        System.out.println("非篡改的方法".concat(String.valueOf(i)));
        System.out.println(token);
        token = token.replaceFirst("y", "x");
        final int i1 = tokenUtil.validToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlcyI6WyJyb2xlMiIsInJvbGUiXSwiaXNzIjoicmVwcmludGVtcHMiLCJleHAiOjE2MjA3ODUyNzUsImlhdCI6MTYyMDc4NDY3NSwidXNlcm5hbWUiOiJ1c2VyIn0.CEPQ1gmKAT0_c_3wQ_OpuFyHqyGkCsuoU1JtD8Oolqc");
        System.out.println("第二次篡改后".concat(String.valueOf(i1)));
        System.out.println(token);
        assert i1 != i;

    }

    // 确认访问 闲置时间大于x驱逐 有效
    @Test
    void testGetTokenCache(){
        // tokenCache.put("star", "cecilia");
        System.out.println(tokenCache.get("star"));
    }


}
