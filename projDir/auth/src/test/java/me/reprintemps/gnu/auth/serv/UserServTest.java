package me.reprintemps.gnu.auth.serv;

import me.reprintemps.gnu.auth.mod.Res;
import me.reprintemps.gnu.auth.mod.dto.RegisterFormDto;
import me.reprintemps.gnu.auth.mod.entity.UserEntity;
import me.reprintemps.gnu.auth.repo.UserRepoJpa;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
public class UserServTest {

    @Autowired
    private RegisterServ registerServ;

    @Autowired
    private UserRepoJpa userRepoJpa;

    // 测试添加注册
    @Test
    void testPost(){
        final HashMap<Integer, List<String>> register = new HashMap<Integer, List<String>>();
        final ArrayList<String> eyeRole = new ArrayList<>();
        final ArrayList<String> oaRole = new ArrayList<>();
        eyeRole.add("role1");
        eyeRole.add("role2");
        oaRole.add("role3");
        register.put(1, eyeRole);
        register.put(2, oaRole);

        final Res result = registerServ.register(new RegisterFormDto().setUsername("陈清扬").setPassword("123").setRoleMap(register));
        assert result.getCode().equals(1);
    }

    // 测试删除
    @Test
    void testDelete(){
        final ArrayList<String> uidList = new ArrayList<>();
        uidList.add("be7d6888-ff55-4cc2-a48b-cf873df9ddc8");
        final Res delete = registerServ.delete(uidList);
        assert delete.getCode().equals(1);
    }

    @Test
    void testDeletedUseful(){
        final List<UserEntity> all = userRepoJpa.findAll();
        System.out.println(all.size());
        assert all.size() == 0;
    }

}
