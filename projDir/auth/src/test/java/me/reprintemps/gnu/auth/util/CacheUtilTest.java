package me.reprintemps.gnu.auth.util;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CacheUtilTest {

    @Autowired
    private CacheUtil<String> ehCacheUtil;

    // 测试缓存是否正常工作
    @Test
    void testCache(){
        final int token = ehCacheUtil.post("123", "token");
        System.out.println(token);
        final String s = ehCacheUtil.get("123");
        System.out.println(s);
    }
}
